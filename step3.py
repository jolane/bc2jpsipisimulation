#Moore-v28r3p1
from Gaudi.Configuration import *
#inputfile = "Gauss-14143013-5ev-20230405.sim"
import glob
#inputfile = glob.glob("Boole*.digi")[0]
inputfile = "Boole.digi"
importOptions("$APPCONFIGOPTS/L0App/L0AppSimProduction.py")
importOptions("$APPCONFIGOPTS/L0App/L0AppTCK-0x18a4.py")
importOptions("$APPCONFIGOPTS/L0App/ForceLUTVersionV8.py")
importOptions("$APPCONFIGOPTS/L0App/DataType-2017.py")
dddb = "dddb-20170721-3"
condb = "sim-20190128-vc-md100"

from Configurables import RawEventDump


from Configurables import StoreExplorerAlg

StoreExplorerAlg().Load=True


from GaudiConf import IOHelper

from Configurables import LHCbApp, Moore, L0App

#i0x00a10045
#Moore().UseTCK = True
#Moore().InitialTCK = "0x18a4"
#Moore().Split = "Hlt1"
L0App().outputFile = "L0.digi"
IOHelper('ROOT').inputFiles([inputfile])
#L0App().ForceSingleL0Configuration = True
#L0App().CheckOdin  = False

#LHCbApp().DDDBtag = 'dddb-20170721-3'
#LHCbApp().CondDBtag = 'sim-20170721-2-vc-md100'


#LHCbApp().CondDBtag ='upgrade/sim-20190912-vc-md100'
#LHCbApp().DDDBtag =  'upgrade/dddb-20190912'



LHCbApp().CondDBtag = condb
LHCbApp().DDDBtag =  dddb



LHCbApp().EvtMax = -1

LHCbApp().DataType="2016"


#output = "Gauss-{}-5ev-20230329.xgen".format(eventnumber)
#output = "Gauss.xgen"
#print(output)

ApplicationMgr().TopAlg+=[StoreExplorerAlg(), RawEventDump()]


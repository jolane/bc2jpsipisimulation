import numpy as np
import pandas as pd
import pickle as pkl
import matplotlib as mpl
mpl.use("Agg")
from matplotlib import pyplot as plt
try:
    import gnuplotlib as gp
except Exception as err:
    print(err)
import os
import uproot

def gplot(x, y):
    try:
        gp.plot(np.array(x), np.array(y), terminal = "dumb (80, 60)")
    except Exception as err:
        r = ''

def plot(x,y,output, xlabel = "", ylabel = "", plotType="scatter"):
    gplot(x, y)
    fig, ax = plt.subplots(1,1)
    if plotType=="scatter":
        pl = ax.scatter(x, y)
    if plotType == "line":
        pl = ax.plot(x, y)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    fig.savefig(output, dpi=300)
    plt.close()

def hist_plot(x, bins, output, xlabel="", ylabel = "", normalise=False):
    n, b = np.histogram(x, bins=bins, density=normalise)
    gplot(np.linspace(min(b), max(b), len(n)), n)
    fig, ax = plt.subplots(1,1)
    bw = (b[1] - b[0])
    bar = ax.bar(np.linspace(min(b), max(b), len(n)), n, width=0.7 * bw)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(f"{ylabel}/{bw:.3f}")
    fig.savefig(output, dpi=300)
    plt.close()

def hist_plot_term(x, bins, normalise=False):
    n, b = np.histogram(x, bins=bins, density=normalise)
    gplot(np.linspace(min(b), max(b), len(n)), n)


def plot2D(x, y, z, output, xlabel="", ylabel="", zlabel = "", markerSize =1):
    print("(x,y) projection")
    gplot(x,y)
    print("(x, z) projecion")
    gplot(x,z)
    print("(y, z) projecion")
    gplot(y,z)
    
    fig, ax = plt.subplots(1,1)
    sc = ax.scatter(x, y, c=z, s=markerSize)
    cb = plt.colorbar(sc, ax=ax)
    cb.set_label(zlabel)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    fig.savefig(output, dpi=300)
    plt.close()


def hist_2D(x, y, bins, output, xlabel="", ylabel=""):
    n, bx, by = np.histogram2d(x, y, bins=bins)
    fig, ax = plt.subplots(1,1)
    sc = ax.imshow(n, origin="lower")
    cb = plt.colorbar(sc, ax=ax)
    bwx = (bx[1] - bx[0])
    bwy = (by[1] - by[0])
    cb.set_label(rf"$N/({bwx:.3f}\\times {bwy:.3f})$")
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    fig.savefig(output, dpi=300)
    plt.close()

def pkl_save(obj, output):
    with open(output, "wb") as f:
        pkl.dump(obj, f)

def pkl_load(fileName):
    return pkl.load(open(fileName, "rb"))






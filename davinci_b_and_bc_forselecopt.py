#adapted from Starterkit second lessons 02.optimized.py
from Configurables import DaVinci
from GaudiConf import IOHelper

# Load algorithms
from Configurables import CombineParticles
from Configurables import DecayTreeTuple, TupleToolTISTOS, TupleToolRecoStats, TupleToolPropertime
from DecayTreeTuple.Configuration import *

# Load input particles
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseMuons as Muons
from StandardParticles import StdAllLooseKaons as Kaons

#stripping documentation
#http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34/commonparticles/stdallloosepions.html

# Load Selection objects
from PhysConf.Selections import CombineSelection, Combine3BodySelection, FilterSelection, StrippingSelection, AutomaticData, TupleSelection
from PhysConf.Selections import SelectionSequence

#for stripping emulation
from StandardParticles import StdLooseJpsi2MuMu as LooseJpsi

#Input into LooseJpsi is StdAllNoPIDsPions with 	(ADAMASS('J/psi(1S)') < 100.*MeV) & (ADOCACHI2CUT(30,'')) as mass cuts.


#select the pp dimu stripping line
#stream = 'IFT'
stream = 'AllStreams'
#line = 'HeavyIonDiMuonJpsi2MuMuLine'
#line = 'FullDSTDiMuonJpsi2MuMuTOSLine'
line = 'FullDSTDiMuonJpsi2MuMuDetachedLine'
#strip_input = AutomaticData('/Event/Dimuon/Phys/{0}/Particles'.format(line))
strip_input = AutomaticData('/Event/AllStreams/Phys/{0}/Particles'.format(line))

#create the J/psi
Jpsi_sel = FilterSelection(
    'Sel_Jpsi',
    [strip_input],
    Code="(ADMASS('J/psi(1S)') < 50*MeV) & (PT > 500*MeV)"
)

#Select pions: there's no PID cut here
Pion_sel = FilterSelection(
    'Sel_Pions',
    [Pions],
    Code='(TRCHI2DOF < 3) & (PT > 1000*MeV) & (P > 3000*MeV)'
)


B_comb = ' (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm) '

#combine the two particles
#B_comb = '(ADOCA(1,2)<0.5*mm)'

#select the B+ and Bc+ mass range
#B_mother =  'in_range(5.0 * GeV ,  M , 6.5 * GeV) & (VFASPF(VCHI2/VDOF)<5)'
B_mother =  'in_range(6.0 * GeV ,  M , 6.5 * GeV) & (VFASPF(VCHI2/VDOF)<10)'

#Combine Jpsi and pion into B_c
B_sel = CombineSelection(
        'B_sel',
            [Jpsi_sel, Pion_sel],
        #    DecayDescriptor='[psi(2S) -> J/psi(1S) pi+ pi-]cc',
            DecayDescriptor='[B_c+ -> J/psi(1S) pi+]cc',
            CombinationCut=B_comb,
            MotherCut=B_mother,
        )


B_seq = SelectionSequence('B_Seq', TopSelection=B_sel)

#write a second selection sequence for the B+

Kaon_sel = FilterSelection(
    'Sel_Kaon',
    [Kaons],
    Code='(TRCHI2DOF < 3) & (PT > 1000*MeV) & (P > 3000*MeV)'
)

#reuse the B_combiner

#select B+ mass range
Bplus_mother =  'in_range(5.0 * GeV ,  M , 5.6 * GeV) & (VFASPF(VCHI2/VDOF)<10)'

#Combine Jpsi and kaon into B+
Bplus_sel = CombineSelection(
        'Bplus_sel',
            [Jpsi_sel, Kaon_sel],
        #    DecayDescriptor='[psi(2S) -> J/psi(1S) pi+ pi-]cc',
            DecayDescriptor='[B+ -> J/psi(1S) K+]cc',
            CombinationCut=B_comb,
            MotherCut=Bplus_mother,
        )

Bplus_seq = SelectionSequence('Bplus_Seq', TopSelection=Bplus_sel)

#now write different trees for both of them

dtt = DecayTreeTuple('Bctuple')
dtt.Inputs = B_seq.outputLocations()

dttplus = DecayTreeTuple('Bplustuple')
dttplus.Inputs = Bplus_seq.outputLocations()

#dtt.Decay = '[psi(2S) -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi+ ^pi-]CC'
#dtt.setDescriptorTemplate('${psi2s}[psi(2S) -> ${Jpsi}(J/psi(1S) -> ${mum}mu- ${mup}mu+) ${pip}pi+ ${pim}pi-]CC')#better way insted of defining each individually
#dtt.setDescriptorTemplate('${psi2s}(psi(2S) -> ${Jpsi}(J/psi(1S) -> ${mum}mu- ${mup}mu+) ${pip}pi+ ${pim}pi-)')
dtt.setDescriptorTemplate('${Bc}[B_c+ -> ${Jpsi}(J/psi(1S) -> ${mum}mu- ${mup}mu+) ${pi}pi+]CC')

#now the B plus decay descriptor
dttplus.setDescriptorTemplate('${B}[B+ -> ${Jpsi}(J/psi(1S) -> ${mum}mu- ${mup}mu+) ${K}K+]CC')

ttreco = dtt.addTupleTool('TupleToolRecoStats')
ttprimaries = dtt.addTupleTool('TupleToolPrimaries')
ttproptime = dtt.Bc.addTupleTool('TupleToolPropertime')

#now the B plus decay descriptor
ttreco_plus = dttplus.addTupleTool('TupleToolRecoStats')
ttprimaries_plus = dttplus.addTupleTool('TupleToolPrimaries')
ttproptime_plus = dttplus.B.addTupleTool('TupleToolPropertime')


#Check if DLS can be found
#ttreco.Verbose = True
#ttprimaries.Verbose = True
#ttproptime.Verbose = True

#Commented out for LDST run

#add modifications from correspondance with Tom.
mcpvtom = dtt.Bc.addTupleTool('TupleToolPVTrackInfo')
#unnecssary given we set the location in the tool
#mcpvtom.PVsLocation    = 'Rec/Vertex/Primary'
mcpvtom.RefitPVs = True
mcpvtom.TruthMatchPVs = False
#mcpvtom.TruthMatchPVs = True
#dtt.B.addTupleTool('TupleToolPVTrackInfo')

#Bplus addition
mcpvtom_plus = dttplus.B.addTupleTool('TupleToolPVTrackInfo')
#unnecssary given we set the location in the tool
#mcpvtom.PVsLocation    = 'Rec/Vertex/Primary'
mcpvtom_plus.RefitPVs = True
mcpvtom_plus.TruthMatchPVs = False


track_tool = dtt.addTupleTool('TupleToolTrackInfo')
track_tool.Verbose = True

#Bplus
track_tool_plus = dttplus.addTupleTool('TupleToolTrackInfo')
track_tool_plus.Verbose = True

#Add event info
etuple=EventTuple()
etuple.ToolList=["TupleToolEventInfo"]

#Add TISTOS info
tistos = dtt.Bc.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos.VerboseL0=True
tistos.VerboseHlt1=True
tistos.VerboseHlt2=True
tistos.TriggerList=["L0MuonDecision", "L0DiMuonDecision", "Hlt1DiMuonHighMassDecision", "Hlt1TrackMuonDecision",  "Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision", "Hlt1L0AnyDecision", "Hlt2Topo3BodyDecision", "Hlt2TopoMu3BodyDecision", "Hlt2Topo3BodyBBDTDecision", "Hlt2TopoMu3BodyBBDTDecision", "Hlt2SingleMuonDecision", "Hlt2DiMuonDetachedDecision"]

#Add TISTOS info plus
tistos_plus = dttplus.B.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos_plus.VerboseL0=True
tistos_plus.VerboseHlt1=True
tistos_plus.VerboseHlt2=True
tistos_plus.TriggerList=["L0MuonDecision", "L0DiMuonDecision", "Hlt1DiMuonHighMassDecision", "Hlt1TrackMuonDecision",  "Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision", "Hlt1L0AnyDecision", "Hlt2Topo3BodyDecision", "Hlt2TopoMu3BodyDecision", "Hlt2Topo3BodyBBDTDecision", "Hlt2TopoMu3BodyBBDTDecision", "Hlt2SingleMuonDecision", "Hlt2DiMuonDetachedDecision"]


#   "Hlt1TrackMVALooseDecision","Hlt1TwoTrackMVALooseDecision",

tistos2 = dtt.Jpsi.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos2.VerboseL0=True
tistos2.VerboseHlt1=True
tistos2.VerboseHlt2=True
tistos2.TriggerList=["L0MuonDecision","L0DiMuonDecision","Hlt1DiMuonHighMassDecision","Hlt2DiMuonDetachedJPsiDecision"]

#add Jpsi information
tistos2_plus = dttplus.Jpsi.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos2_plus.VerboseL0=True
tistos2_plus.VerboseHlt1=True
tistos2_plus.VerboseHlt2=True
tistos2_plus.TriggerList=["L0MuonDecision","L0DiMuonDecision","Hlt1DiMuonHighMassDecision","Hlt2DiMuonDetachedJPsiDecision"]


#Add DecayTreeFitter
dtt.Bc.addTupleTool('TupleToolDecayTreeFitter/DTF_ConsJpsi')#DTF_ConsJspi is the name we give it, since it is used to constrain Jpsi
dtt.Bc.DTF_ConsJpsi.constrainToOriginVertex = True
dtt.Bc.DTF_ConsJpsi.Verbose = True
dtt.Bc.DTF_ConsJpsi.daughtersToConstrain = ['J/psi(1S)'] #this one takes a list
dtt.Bc.DTF_ConsJpsi.UpdateDaughters = True

#Add DecayTreeFitter for the Bplus
dttplus.B.addTupleTool('TupleToolDecayTreeFitter/DTF_ConsJpsi')#DTF_ConsJspi is the name we give it, since it is used to constrain Jpsi
dttplus.B.DTF_ConsJpsi.constrainToOriginVertex = True
dttplus.B.DTF_ConsJpsi.Verbose = True
dttplus.B.DTF_ConsJpsi.daughtersToConstrain = ['J/psi(1S)'] #this one takes a list
dttplus.B.DTF_ConsJpsi.UpdateDaughters = True



#vertex isolation Tuple Tool https://gitlab.cern.ch/lhcb/Analysis/-/blob/run2-patches/Phys/DecayTreeTuple/src/TupleToolVtxIsoln.cpp?fbclid=IwAR23xTJ8ZbsnIeaRUfdknD6HgUELTigOIGN-cy4fTqoOA5uW0zX1h_QBsyk
#https://github.com/ibab/lhcb-b2dmumu/blob/master/lhcb/cmtuser/DaVinci_v36r5/Phys/DecayTreeTuple/src/TupleToolVtxIsoln.h

VtxIso = dtt.Bc.addTupleTool('TupleToolVtxIsoln')
VtxIso.Verbose = True

#add Bplus
VtxIso_plus = dttplus.B.addTupleTool('TupleToolVtxIsoln')
VtxIso_plus.Verbose = True


#add MC truth information
#mctruth = dtt.B.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]

#Truth matching algorithms from Tom Hadavizadeh (25/11/2022)

#from Configurables import TrackAssociator
#velotrassoc = TrackAssociator()
#velotrassoc.TracksInContainer = 'Rec/Track/FittedHLT1VeloTracks'

#from Configurables import LoKi__Track2MC
#lokitrassoc = LoKi__Track2MC()
#lokitrassoc.Tracks = ['Rec/Track/FittedHLT1VeloTracks']

#from Configurables import LoKi__PV2MC
#pv2mc = LoKi__PV2MC()
#pv2mc.MCVertices = 'MC/Vertices'
#pv2mc.Primaries =  'Rec/Vertex/Primary'

#new algorithms
#DaVinci().UserAlgorithms += [B_seq.sequence(),velotrassoc,lokitrassoc,  dtt, etuple]

#old algorithms
DaVinci().UserAlgorithms += [B_seq.sequence(), Bplus_seq.sequence(), dtt, dttplus, etuple]

# DaVinci configuration
DaVinci().InputType = 'LDST'
DaVinci().TupleFile = 'Btuple_Selection.root'
#DaVinci().PrintFreq = 1000
#DaVinci().DataType = '2018'
DaVinci().Simulation = True
#DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1

#LDST Files
DaVinci().DataType = '2016'
#DaVinci().CondDBtag = 'sim-20190430-vc-md100'

#DST
#DaVinci().CondDBtag = 'sim-20160614-1-vc-md100'
#DaVinci().DDDBtag = 'dddb-20170721-3'

#for 2018 dimuon data stream MD
#DaVinci().CondDBtag = 'cond-20200921'
#DaVinci().DDDBtag = 'dddb-20210528-8'

dddb = "dddb-20210528-8"
condb = "sim-20201113-8-vc-md100-Sim10"


#for 2018 dimuon data stream MD
#DaVinci().CondDBtag = 'cond-20191004-1'
DaVinci().CondDBtag = condb
#DaVinci().DDDBtag = 'dddb-20190206-3'
DaVinci().DDDBtag = dddb


#DaVinci().DataType = '2012'
#DaVinci().CondDBtag = 'sim-20160321-2-vc-md100'
#DaVinci().DDDBtag = 'dddb-20170721-2'

#commeted out EJW
#DaVinci().CondDBtag = 'sim-20190430-vc-mu100' #get from header of .py file with LFNs downloaded from bookkeeping

#for simulation
#DaVinci().DDDBtag = 'dddb-20170721-3' #very important for simulations, this is detector configuration

#added for local testing
#from Configurables import CondDB
#CondDB(LatestGlobalTagByDataType='2018')#Use the local input data
import pickle as pkl
#inputfile = "000000.AllStreams.ldst"
#IOHelper('ROOT').inputFiles([inputfile])

urls = pkl.load(open("urls_20.pkl", "rb"))
print(urls)
IOHelper('ROOT').inputFiles(urls)

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--genJob", type=int, default=-1)
parser.add_argument("--jobStart", type=int, default=0)
parser.add_argument("--jobEnd", type=int, default=10)

args = parser.parse_args()
if args.genJob>=0:
    job1 = jobs(args.genJob)
    j = Job()
    j.application = Executable()
    j.application.exe = File('step_2_8.sh')
    #j.inputfiles = [job1.subjobs(0).outputfiles[0]] +
    #j.splitter.values = [{"AMPGEN_SEED":"1"}, {"AMPGEN_SEED":"2"}]
    j.inputfiles = [LocalFile('step{}.py'.format(i)) for i in range(2, 9)]
    d = []




    d = [[s.outputfiles[0]] + [LocalFile('step{}.py'.format(i)) for i in range(2, 9)] for s in list(job1.subjobs)[args.jobStart:args.jobEnd]] 
    j.splitter = GenericSplitter()
    j.splitter.attribute = "inputfiles"
    j.splitter.values = d

    #j.backend = Dirac()
    j.backend = Local()
    j.outputfiles = [DiracFile("*AllStreams*.ldst")]
    j.submit()

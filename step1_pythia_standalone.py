from Gaudi.Configuration import *

# Step 1 Sim10b - 2018 - MD - Nu1.6 (Lumi 4 at 25ns) - 25ns spillover - BcVegPy w Pythia8(148610/Sim10b) : Gauss-v56r2
# System config: x86_64_v2-centos7-gcc11-opt MC TCK:
# Options: $APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2018-nu1.6.py;
# $APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py;$APPCONFIGOPTS/Gauss/DataType-2018.py;
# $APPCONFIGOPTS/Gauss/RICHRandomHits.py;
# $DECFILESROOT/options/@{eventType}.py;
# $LBBCVEGPYROOT/options/BcVegPyPythia8.py;
# $APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmOpt2.py 
# Options format: Multicore: N
# DDDB: dddb-20210528-8 Condition DB: sim-20201113-8-vc-md100-Sim10 DQTag:

eventType = 14143013 #Bc to j/psi pi
importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2018-nu1.6.py")
importOptions("$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py")
importOptions("$APPCONFIGOPTS/Gauss/DataType-2018.py")

importOptions("$GAUSSOPTS/GenStandAlone.py")
#importOptions("$APPCONFIGOPTS/Gauss/RICHRandomHits.py")
#importOptions(f"$DECFILESROOT/options/{eventType}.py")
importOptions(f"{eventType}_pythia.py")
#importOptions("$LBBCVEGPYROOT/options/BcVegPyPythia8.py")
importOptions("Pythia8.py")
importOptions("Pythia8_IncbUserHook.py")
#importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmOpt2.py")
#importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")

dddb = "dddb-20210528-8"
condb = "sim-20201113-8-vc-md100-Sim10"

from Configurables import LHCbApp, Gauss
LHCbApp().CondDBtag = condb
LHCbApp().DDDBtag =  dddb

from Configurables import Pythia8Production
Pythia8Production().UserTuning = "LHCbDefault.cmd"
from Gauss.Configuration import GenInit
GaussGen = GenInit("GaussGen")
#GaussGen.FirstEventNumber = 1

#GaussGen.RunNumber = 1082

LHCbApp().EvtMax = 1
LHCbApp().DataType="2018"

#Generator.Members +=  { "DumpHepMCTree/Dump" } 
#Generator.Members +=  { "DumpMC/Dump" } 
    
Gauss().OutputType="XSIM"

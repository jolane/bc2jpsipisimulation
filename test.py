from Gauss.Configuration import *


#--Number of events
# We will set number of events separately in other file, we want to make
# sure we pick right number.
nEvts = 1
LHCbApp().EvtMax = nEvts

#Create local file dbtags.py with content:

from Configurables import LHCbApp
#LHCbApp().DDDBtag   = "TORCH"
#LHCbApp().CondDBtag = "sim-20200515-vc-mu100"
from Configurables import Gauss, CondDB
#CondDB().Upgrade = True


#Gauss().DetectorGeo  = { "Detectors": ['VP', 'UT', 'FT-NoShield', 'Torch', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }
#Gauss().DetectorSim  = { "Detectors": ['VP', 'UT', 'FT-NoShield', 'Torch', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }
#Gauss().DetectorMoni = { "Detectors": ['VP', 'UT', 'FT-NoShield', 'Torch', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }


Gauss().DataType = "2018"
#Gauss().UpgradeRICHSimRunOption = 'HGV'

dddb = "dddb-20210528-8"
condb = "sim-20201113-8-vc-md100-Sim10"

from Configurables import LHCbApp
LHCbApp().CondDBtag = condb
LHCbApp().DDDBtag =  dddb


LHCbApp().DataType="2018"

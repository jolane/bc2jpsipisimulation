# file /tmp/tmp.01bE2pn7Rj/DBASE/Gen/DecFiles/v32r6/options/14143013.py generated: Thu, 13 Apr 2023 14:00:34
#
# Event Type: 14143013
#
# ASCII decay Descriptor: [B_c+ -> (J/psi(1S) -> mu+ mu- {,gamma} {,gamma}) pi+]cc
#
genAlgName="Generation"
from Configurables import Generation
Generation(genAlgName).EventType = 14143013
Generation(genAlgName).SampleGenerationTool = "Special"
from Configurables import Special
Generation(genAlgName).addTool( Special )
#Generation(genAlgName).Special.ProductionTool = "BcVegPyProduction"
Generation(genAlgName).Special.ProductionTool = "Pythia8Production"
Generation(genAlgName).PileUpTool = "FixedLuminosityForRareProcess"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
#ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Bc_Jpsipi,mm=WeightedBcVegPy,DecProdCut.dec"
ToolSvc().EvtGenDecay.UserDecayFile = "Bc_Jpsipi,mm=WeightedBcVegPy,DecProdCut.dec"
Generation(genAlgName).Special.CutTool = "BcDaughtersInLHCb"

import sys
import pickle as pkl
try:
    jobId = sys.argv[1]
except:
    jobId = 20
j = jobs(jobId)
urls = []
for sj in j.subjobs.select(status="completed"):
    try:
        urls += [sj.outputfiles[0].accessURL()[0]]
    except Exception as err:
        print(f"No url for {j.id}.{sj.id}, {err}")

if urls:
    print(f"Have {len(urls)} for {j.id}")
    pkl.dump(urls, open(f"urls_{jobId}.pkl", "wb"))

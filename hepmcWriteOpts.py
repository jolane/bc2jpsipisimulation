from Gauss.Configuration import *
from Generators.GeneratorsConf import WriteHepMCAsciiFile
LHCbApp().EvtMax = 10
# Disable all Gauss normal output:
Gauss().Histograms = "NONE"
#Gauss().Output = "NONE"
Gauss().EnablePack = False
GenMonitor = GaudiSequencer( "GenMonitor" )
GenMonitor.Members += [ "WriteHepMCAsciiFile" ]
#path to the file where the events are output in HepMC format
WriteHepMCAsciiFile().Output = "test.hepmc"

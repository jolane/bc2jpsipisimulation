import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--nJobs", type=int, default=1)
parser.add_argument("--nEvents", type=int, default=10)
parser.add_argument("--genType", type=str, default="bcvegpy")
args=parser.parse_args()
j = Job()
myApp = GaudiExec()
#myApp = prepareGaudiExec('Gauss','v56r3', myPath='.')

nJobs = args.nJobs
nEventsPerJob = args.nEvents
genType = args.genType
#myApp.directory="./GaussDev_v56r3"
myApp.directory="./GaussDev_v49r12"
j.application = myApp
j.application.options = [f"step1_{genType}.py"]
j.application.platform = 'x86_64_v2-centos7-gcc11-opt'
j.inputfiles = [LocalFile("LHCbDefault.cmd"), LocalFile("Pythia8_IncbUserHook.py"), LocalFile("Pythia8.py"), LocalFile(f"14143013_{genType}.py"), LocalFile("Bc_Jpsipi,mm=WeightedBcVegPy,DecProdCut.dec")]
j.outputfiles = [DiracFile("*.xsim")]
j.splitter = GaussSplitter(eventsPerJob = nEventsPerJob, numberOfJobs = nJobs)
j.backend = Interactive()
j.name  = f"Bc2JpsiPi_{genType}_{nEventsPerJob}x{nJobs}"
j.submit()

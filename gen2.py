j = Job(name=f"GaussGen_100x1000_Bc2Jpsipi_pythia8UH" )
myApp = GaudiExec()
#myApp = prepareGaudiExec('Gauss','v56r3', myPath='.')

myApp.directory="./GaussDev_v56r3"
j.application = myApp
j.application.options = ["step1_2.py"]
j.application.platform = 'x86_64_v2-centos7-gcc11-opt'
j.inputfiles = [LocalFile("LHCbDefault.cmd"), LocalFile("Pythia8_IncbUserHook.py"), LocalFile("Pythia8.py")]
j.outputfiles = [DiracFile("*.sim")]
j.splitter = GaussSplitter(eventsPerJob = 100, numberOfJobs = 100)
j.backend = Dirac()

j.submit()

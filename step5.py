#!/usr/bin/env gaudirun.py
#
# Minimal file for running Moore from python prompt
# Syntax is:
#   gaudirun.py ../options/Moore.py
# or just
#   ../options/Moore.py
#

from Gaudi.Configuration import *
import Gaudi.Configuration
from Moore.Configuration import Moore

importOptions("$APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py")
importOptions("$APPCONFIGOPTS/Conditions/TCK-0x617d18a4.py")
importOptions("$APPCONFIGOPTS/Moore/DataType-2017.py")
importOptions("$APPCONFIGOPTS/Moore/MooreSimProductionHlt2.py")
# if you want to generate a configuration, uncomment the following lines:
#Moore().generateConfig = True
#Moore().configLabel = 'Default'
#Moore().ThresholdSettings = 'Commissioning_PassThrough'
#Moore().configLabel = 'ODINRandom acc=0, TELL1Error acc=1'

#Moore().ThresholdSettings = 'Physics_September2012'

Moore().EvtMax = 10000
from Configurables import EventSelector
EventSelector().PrintFreq = 100

Moore().ForceSingleL0Configuration = False

from PRConfig.TestFileDB import test_file_db
input = test_file_db['2012_raw_default']
input.run(configurable=Moore()) 
#Moore().inputFiles = input.filenames
Moore().inputFiles = ["Hlt1.digi"]
Moore().outputFile = "Hlt2.digi"

Moore().Split = 'Hlt1'

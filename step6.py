#Brunel-v54r4
from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/Brunel/DataType-2018.py")
importOptions("$APPCONFIGOPTS/Brunel/MC-WithTruth.py")
importOptions("$APPCONFIGOPTS/Brunel/ldst.py")
importOptions("$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py")

#Options: $APPCONFIGOPTS/Brunel/DataType-2018.py;$APPCONFIGOPTS/Brunel/MC-WithTruth.py;$APPCONFIGOPTS/Brunel/ldst.py;$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py Options format: Multicore: N
from GaudiConf import IOHelper

from Configurables import LHCbApp, Brunel
inputfile = "Hlt2.digi"
from GaudiConf import IOHelper
#Brunel().output_file = "brunel.dst"
IOHelper('ROOT').inputFiles([inputfile])
#



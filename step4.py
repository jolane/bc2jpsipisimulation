#!/usr/bin/env gaudirun.py
#
# Minimal file for running Moore from python prompt
# Syntax is:
#   gaudirun.py ../options/Moore.py
# or just
#   ../options/Moore.py
#
import Gaudi.Configuration
from Moore.Configuration import Moore

from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/Moore/MooreSimProductionForSeparateL0AppStep2015.py")
importOptions("$APPCONFIGOPTS/Conditions/TCK-0x517a18a4.py")
importOptions("$APPCONFIGOPTS/Moore/DataType-2017.py")
importOptions("$APPCONFIGOPTS/Moore/MooreSimProductionHlt1.py")

dddb = "dddb-20170721-3"
condb = "sim-20190128-vc-md100"



# if you want to generate a configuration, uncomment the following lines:
#Moore().generateConfig = True
#Moore().configLabel = 'Default'
#Moore().ThresholdSettings = 'Commissioning_PassThrough'
#Moore().configLabel = 'ODINRandom acc=0, TELL1Error acc=1'

#Moore().ThresholdSettings = 'Physics_September2012'

Moore().EvtMax = -1
from Configurables import EventSelector
EventSelector().PrintFreq = 1

Moore().ForceSingleL0Configuration = False

from PRConfig.TestFileDB import test_file_db
input = test_file_db['2012_raw_default']
input.run(configurable=Moore()) 
#input.filenames = [ '/data/bfys/graven/0x46/'+f.split('/')[-1] for f in input.filenames ]# Gerhard, no personal files in a release please!
#Moore().inputFiles = input.filenames
import glob
inputfile = "L0.digi"
Moore().inputFiles = [inputfile]

# /data/bfys/graven/0x46
#Moore().WriterRequires = [ 'Hlt1' ]  # default is HltDecisionSequence, which Split = 'Hlt1' will remove (maybe it should remove Hlt2 from HltDecisionSequence instead???)
#Moore().outputFile = '/data/bfys/graven/0x46/hlt1.raw'# Gerhard, no personal files in a release please!
Moore().outputFile = "Hlt1.digi"
Moore().Split = 'Hlt1'

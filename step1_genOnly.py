eventType = 14143013 #Bc to j/psi pi
nEvents = 100
from Gaudi.Configuration import *
from Generators.GeneratorsConf import WriteHepMCAsciiFile
# Step 1 Sim10b - 2018 - MD - Nu1.6 (Lumi 4 at 25ns) - 25ns spillover - BcVegPy w Pythia8(148610/Sim10b) : Gauss-v56r2
# System config: x86_64_v2-centos7-gcc11-opt MC TCK:
# Options: $APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2018-nu1.6.py;
# $APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py;$APPCONFIGOPTS/Gauss/DataType-2018.py;
# $APPCONFIGOPTS/Gauss/RICHRandomHits.py;
# $DECFILESROOT/options/@{eventType}.py;
# $LBBCVEGPYROOT/options/BcVegPyPythia8.py;
# $APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmOpt2.py 
# Options format: Multicore: N
# DDDB: dddb-20210528-8 Condition DB: sim-20201113-8-vc-md100-Sim10 DQTag:


importOptions("$GAUSSOPTS/Gauss-Job.py")
importOptions("$GAUSSOPTS/Gauss-2016.py")
importOptions("$GAUSSOPTS/GenStandAlone.py")

importOptions("$DECFILESROOT/options/{}.py".format(eventType))

importOptions("$LBBCVEGPYROOT/options/BcVegPyPythia8.py")
from Configurables import LHCbApp
LHCbApp().EvtMax = nEvents


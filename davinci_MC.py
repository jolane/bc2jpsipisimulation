local=True
from Configurables import DaVinci
from GaudiConf import IOHelper

# Load algorithms
from Configurables import CombineParticles
from Configurables import DecayTreeTuple, TupleToolTISTOS, TupleToolRecoStats, TupleToolPropertime
from DecayTreeTuple.Configuration import *
from Configurables import BackgroundCategory

# Load input particles
from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllLooseMuons as Muons
from StandardParticles import StdAllLooseKaons as Kaons

#stripping documentation
#http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34/commonparticles/stdallloosepions.html

# Load Selection objects
from PhysConf.Selections import CombineSelection, Combine3BodySelection, FilterSelection, StrippingSelection, AutomaticData, TupleSelection
from PhysConf.Selections import SelectionSequence

#for stripping emulation
from StandardParticles import StdLooseJpsi2MuMu as LooseJpsi

#Input into LooseJpsi is StdAllNoPIDsPions with 	(ADAMASS('J/psi(1S)') < 100.*MeV) & (ADOCACHI2CUT(30,'')) as mass cuts.


#select the pp dimu stripping line
#stream = 'IFT'
stream = 'AllStreams'
#line = 'HeavyIonDiMuonJpsi2MuMuLine'
#line = 'FullDSTDiMuonJpsi2MuMuTOSLine'
line = 'FullDSTDiMuonJpsi2MuMuDetachedLine'
strip_input = AutomaticData('/Event/AllStreams/Phys/{0}/Particles'.format(line))
#strip_input = AutomaticData('/Event/AllStreams/Phys/{0}/Particles'.format(line))

#create the J/psi
Jpsi_sel = FilterSelection(
    'Sel_Jpsi',
    [strip_input],
    Code="(ADMASS('J/psi(1S)') < 50*MeV) & (PT > 500*MeV)"
)

#Select pions: there's no PID cut here
Pion_sel = FilterSelection(
    'Sel_Pions',
    [Pions],
    Code='(TRCHI2DOF < 3) & (PT > 1000*MeV) & (P > 3000*MeV)'
)


B_comb = ' (ADOCA(1,3)<0.5*mm) & (ADOCA(2,3)<0.5*mm) '

#combine the two particles
#B_comb = '(ADOCA(1,2)<0.5*mm)'

#select the B+ and Bc+ mass range
#B_mother =  'in_range(5.0 * GeV ,  M , 6.5 * GeV) & (VFASPF(VCHI2/VDOF)<5)'
B_mother =  'in_range(6.0 * GeV ,  M , 6.5 * GeV) & (VFASPF(VCHI2/VDOF)<10)'

#Combine Jpsi and pion into B_c
BcJpsiPi_sel = CombineSelection(
        'BcJpsiPi_sel',
            [Jpsi_sel, Pion_sel],
        #    DecayDescriptor='[psi(2S) -> J/psi(1S) pi+ pi-]cc',
            DecayDescriptor='[B_c+ -> J/psi(1S) pi+]cc',
            CombinationCut=B_comb,
            MotherCut=B_mother,
        )


BcJpsiPi_seq = SelectionSequence('BcJpsiPi_Seq', TopSelection=BcJpsiPi_sel)

#write a second selection sequence for the B+

Kaon_sel = FilterSelection(
    'Sel_Kaon',
    [Kaons],
    Code='(TRCHI2DOF < 3) & (PT > 1000*MeV) & (P > 3000*MeV)'
)

#reuse the B_combiner

#select B+ mass range
Bplus_mother =  'in_range(5.0 * GeV ,  M , 5.6 * GeV) & (VFASPF(VCHI2/VDOF)<10)'

#Combine Jpsi and kaon into B+
BJpsiK_sel = CombineSelection(
        'BJpsiK_sel',
            [Jpsi_sel, Kaon_sel],
        #    DecayDescriptor='[psi(2S) -> J/psi(1S) pi+ pi-]cc',
            DecayDescriptor='[B+ -> J/psi(1S) K+]cc',
            CombinationCut=B_comb,
            MotherCut=Bplus_mother,
        )

BJpsiK_seq = SelectionSequence('BJpsiK_Seq', TopSelection=BJpsiK_sel)


BcJpsiK_sel = CombineSelection(
        'BcJpsiK_sel',
            [Jpsi_sel, Kaon_sel],
        #    DecayDescriptor='[psi(2S) -> J/psi(1S) pi+ pi-]cc',
            DecayDescriptor='[B_c+ -> J/psi(1S) K+]cc',
            CombinationCut=B_comb,
            MotherCut=B_mother,
        )


BcJpsiK_seq = SelectionSequence('BcJpsiK_Seq', TopSelection=BcJpsiK_sel)

BJpsiPi_sel = CombineSelection(
        'BJpsiPi_sel',
            [Jpsi_sel, Pion_sel],
        #    DecayDescriptor='[psi(2S) -> J/psi(1S) pi+ pi-]cc',
            DecayDescriptor='[B+ -> J/psi(1S) pi+]cc',
            CombinationCut=B_comb,
            MotherCut=Bplus_mother,
        )

BJpsiPi_seq = SelectionSequence('BJpsiPi_Seq', TopSelection=BJpsiPi_sel)



#now write different trees for both of them

dtt_BcJpsiPi = DecayTreeTuple('BcJpsiPi')
dtt_BcJpsiPi.Inputs = BcJpsiPi_seq.outputLocations()
dtt_BcJpsiK = DecayTreeTuple('BcJpsiK')
dtt_BcJpsiK.Inputs = BcJpsiK_seq.outputLocations()



dtt_BJpsiK = DecayTreeTuple('BJpsiK')
dtt_BJpsiK.Inputs = BJpsiK_seq.outputLocations()


dtt_BJpsiPi = DecayTreeTuple('BJpsiPi')
dtt_BJpsiPi.Inputs = BJpsiPi_seq.outputLocations()



#dtt.Decay = '[psi(2S) -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi+ ^pi-]CC'
#dtt.setDescriptorTemplate('${psi2s}[psi(2S) -> ${Jpsi}(J/psi(1S) -> ${mum}mu- ${mup}mu+) ${pip}pi+ ${pim}pi-]CC')#better way insted of defining each individually
#dtt.setDescriptorTemplate('${psi2s}(psi(2S) -> ${Jpsi}(J/psi(1S) -> ${mum}mu- ${mup}mu+) ${pip}pi+ ${pim}pi-)')
dtt_BcJpsiPi.setDescriptorTemplate('${Bc}[B_c+ -> ${Jpsi}(J/psi(1S) -> ${mum}mu- ${mup}mu+) ${pi}pi+]CC')
dtt_BcJpsiK.setDescriptorTemplate('${Bc}[B_c+ -> ${Jpsi}(J/psi(1S) -> ${mum}mu- ${mup}mu+) ${K}K+]CC')

#now the B plus decay descriptor
dtt_BJpsiK.setDescriptorTemplate('${B}[B+ -> ${Jpsi}(J/psi(1S) -> ${mum}mu- ${mup}mu+) ${K}K+]CC')
dtt_BJpsiPi.setDescriptorTemplate('${B}[B+ -> ${Jpsi}(J/psi(1S) -> ${mum}mu- ${mup}mu+) ${pi}pi+]CC')

ttreco_BcJpsiPi = dtt_BcJpsiPi.addTupleTool('TupleToolRecoStats')
ttprimaries_BcJPsiPi = dtt_BcJpsiPi.addTupleTool('TupleToolPrimaries')
ttproptime_BcJpsiPi = dtt_BcJpsiPi.Bc.addTupleTool('TupleToolPropertime')

ttreco_BcJpsiK = dtt_BcJpsiK.addTupleTool('TupleToolRecoStats')
ttprimaries_BcJPsiPi = dtt_BcJpsiK.addTupleTool('TupleToolPrimaries')
ttproptime_BcJpsiK = dtt_BcJpsiK.Bc.addTupleTool('TupleToolPropertime')

#now the B plus decay descriptor
ttreco_BJpsiK = dtt_BJpsiK.addTupleTool('TupleToolRecoStats')
ttprimaries_BJpsiK = dtt_BJpsiK.addTupleTool('TupleToolPrimaries')
ttproptime_BJpsiK = dtt_BJpsiK.B.addTupleTool('TupleToolPropertime')
#now the B plus decay descriptor
ttreco_BJpsiPi = dtt_BJpsiPi.addTupleTool('TupleToolRecoStats')
ttprimaries_BJpsiPi = dtt_BJpsiPi.addTupleTool('TupleToolPrimaries')
ttproptime_BJpsiPi = dtt_BJpsiPi.B.addTupleTool('TupleToolPropertime')




#Check if DLS can be found
#ttreco.Verbose = True
#ttprimaries.Verbose = True
#ttproptime.Verbose = True

#Commented out for LDST run

#add modifications from correspondance with Tom.
mcpvtom_BcJpsiPi = dtt_BcJpsiPi.Bc.addTupleTool('TupleToolPVTrackInfo')
#unnecssary given we set the location in the tool
#mcpvtom.PVsLocation    = 'Rec/Vertex/Primary'
mcpvtom_BcJpsiPi.RefitPVs = True
mcpvtom_BcJpsiPi.TruthMatchPVs = False
#mcpvtom.TruthMatchPVs = True
#dtt.B.addTupleTool('TupleToolPVTrackInfo')

mcpvtom_BcJpsiK = dtt_BcJpsiK.Bc.addTupleTool('TupleToolPVTrackInfo')
#unnecssary given we set the location in the tool
#mcpvtom.PVsLocation    = 'Rec/Vertex/Primary'
mcpvtom_BcJpsiK.RefitPVs = True
mcpvtom_BcJpsiK.TruthMatchPVs = False

#Bplus addition
mcpvtom_BJpsiK = dtt_BJpsiK.B.addTupleTool('TupleToolPVTrackInfo')
#unnecssary given we set the location in the tool
#mcpvtom.PVsLocation    = 'Rec/Vertex/Primary'
mcpvtom_BJpsiK.RefitPVs = True
mcpvtom_BJpsiK.TruthMatchPVs = False

mcpvtom_BJpsiPi = dtt_BJpsiPi.B.addTupleTool('TupleToolPVTrackInfo')
#unnecssary given we set the location in the tool
#mcpvtom.PVsLocation    = 'Rec/Vertex/Primary'
mcpvtom_BJpsiPi.RefitPVs = True
mcpvtom_BJpsiPi.TruthMatchPVs = False



track_tool_BcJpsiPi = dtt_BcJpsiPi.addTupleTool('TupleToolTrackInfo')
track_tool_BcJpsiPi.Verbose = True

track_tool_BcJpsiK = dtt_BcJpsiK.addTupleTool('TupleToolTrackInfo')
track_tool_BcJpsiK.Verbose = True


#Bplus
track_tool_BJpsiK = dtt_BJpsiK.addTupleTool('TupleToolTrackInfo')
track_tool_BJpsiK.Verbose = True

track_tool_BJpsiPi = dtt_BJpsiPi.addTupleTool('TupleToolTrackInfo')
track_tool_BJpsiPi.Verbose = True


#Add event info
etuple=EventTuple()
etuple.ToolList=["TupleToolEventInfo"]

#Add TISTOS info
tistos_BcJpsiPi = dtt_BcJpsiPi.Bc.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos_BcJpsiPi.VerboseL0=True
tistos_BcJpsiPi.VerboseHlt1=True
tistos_BcJpsiPi.VerboseHlt2=True
tistos_BcJpsiPi.TriggerList=["L0MuonDecision", "L0DiMuonDecision", "Hlt1DiMuonHighMassDecision", "Hlt1TrackMuonDecision",  "Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision", "Hlt1L0AnyDecision", "Hlt2Topo3BodyDecision", "Hlt2TopoMu3BodyDecision", "Hlt2Topo3BodyBBDTDecision", "Hlt2TopoMu3BodyBBDTDecision", "Hlt2SingleMuonDecision", "Hlt2DiMuonDetachedDecision"]

tistos_BcJpsiK = dtt_BcJpsiK.Bc.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos_BcJpsiK.VerboseL0=True
tistos_BcJpsiK.VerboseHlt1=True
tistos_BcJpsiK.VerboseHlt2=True
tistos_BcJpsiK.TriggerList=["L0MuonDecision", "L0DiMuonDecision", "Hlt1DiMuonHighMassDecision", "Hlt1TrackMuonDecision",  "Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision", "Hlt1L0AnyDecision", "Hlt2Topo3BodyDecision", "Hlt2TopoMu3BodyDecision", "Hlt2Topo3BodyBBDTDecision", "Hlt2TopoMu3BodyBBDTDecision", "Hlt2SingleMuonDecision", "Hlt2DiMuonDetachedDecision"]

#Add TISTOS info plus
tistos_BJpsiK = dtt_BJpsiK.B.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos_BJpsiK.VerboseL0=True
tistos_BJpsiK.VerboseHlt1=True
tistos_BJpsiK.VerboseHlt2=True
tistos_BJpsiK.TriggerList=["L0MuonDecision", "L0DiMuonDecision", "Hlt1DiMuonHighMassDecision", "Hlt1TrackMuonDecision",  "Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision", "Hlt1L0AnyDecision", "Hlt2Topo3BodyDecision", "Hlt2TopoMu3BodyDecision", "Hlt2Topo3BodyBBDTDecision", "Hlt2TopoMu3BodyBBDTDecision", "Hlt2SingleMuonDecision", "Hlt2DiMuonDetachedDecision"]

tistos_BJpsiPi = dtt_BJpsiPi.B.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos_BJpsiPi.VerboseL0=True
tistos_BJpsiPi.VerboseHlt1=True
tistos_BJpsiPi.VerboseHlt2=True
tistos_BJpsiPi.TriggerList=["L0MuonDecision", "L0DiMuonDecision", "Hlt1DiMuonHighMassDecision", "Hlt1TrackMuonDecision",  "Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision", "Hlt1L0AnyDecision", "Hlt2Topo3BodyDecision", "Hlt2TopoMu3BodyDecision", "Hlt2Topo3BodyBBDTDecision", "Hlt2TopoMu3BodyBBDTDecision", "Hlt2SingleMuonDecision", "Hlt2DiMuonDetachedDecision"]



#   "Hlt1TrackMVALooseDecision","Hlt1TwoTrackMVALooseDecision",

tistos2_BcJpsiPi = dtt_BcJpsiPi.Jpsi.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos2_BcJpsiPi.VerboseL0=True
tistos2_BcJpsiPi.VerboseHlt1=True
tistos2_BcJpsiPi.VerboseHlt2=True
tistos2_BcJpsiPi.TriggerList=["L0MuonDecision","L0DiMuonDecision","Hlt1DiMuonHighMassDecision","Hlt2DiMuonDetachedJPsiDecision"]
tistos2_BcJpsiK = dtt_BcJpsiK.Jpsi.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos2_BcJpsiK.VerboseL0=True
tistos2_BcJpsiK.VerboseHlt1=True
tistos2_BcJpsiK.VerboseHlt2=True
tistos2_BcJpsiK.TriggerList=["L0MuonDecision","L0DiMuonDecision","Hlt1DiMuonHighMassDecision","Hlt2DiMuonDetachedJPsiDecision"]



#add Jpsi information
tistos2_BJpsiK = dtt_BJpsiK.Jpsi.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos2_BJpsiK.VerboseL0=True
tistos2_BJpsiK.VerboseHlt1=True
tistos2_BJpsiK.VerboseHlt2=True
tistos2_BJpsiK.TriggerList=["L0MuonDecision","L0DiMuonDecision","Hlt1DiMuonHighMassDecision","Hlt2DiMuonDetachedJPsiDecision"]
#add Jpsi information
tistos2_BJpsiPi = dtt_BJpsiPi.Jpsi.addTupleTool(TupleToolTISTOS, name="TupleToolTISTOS")
tistos2_BJpsiPi.VerboseL0=True
tistos2_BJpsiPi.VerboseHlt1=True
tistos2_BJpsiPi.VerboseHlt2=True
tistos2_BJpsiPi.TriggerList=["L0MuonDecision","L0DiMuonDecision","Hlt1DiMuonHighMassDecision","Hlt2DiMuonDetachedJPsiDecision"]




#Add DecayTreeFitter
dtt_BcJpsiPi.Bc.addTupleTool('TupleToolDecayTreeFitter/DTF_ConsJpsi')#DTF_ConsJspi is the name we give it, since it is used to constrain Jpsi
dtt_BcJpsiPi.Bc.DTF_ConsJpsi.constrainToOriginVertex = True
dtt_BcJpsiPi.Bc.DTF_ConsJpsi.Verbose = True
dtt_BcJpsiPi.Bc.DTF_ConsJpsi.daughtersToConstrain = ['J/psi(1S)'] #this one takes a list
dtt_BcJpsiPi.Bc.DTF_ConsJpsi.UpdateDaughters = True

dtt_BcJpsiK.Bc.addTupleTool('TupleToolDecayTreeFitter/DTF_ConsJpsi')#DTF_ConsJspi is the name we give it, since it is used to constrain Jpsi
dtt_BcJpsiK.Bc.DTF_ConsJpsi.constrainToOriginVertex = True
dtt_BcJpsiK.Bc.DTF_ConsJpsi.Verbose = True
dtt_BcJpsiK.Bc.DTF_ConsJpsi.daughtersToConstrain = ['J/psi(1S)'] #this one takes a list
dtt_BcJpsiK.Bc.DTF_ConsJpsi.UpdateDaughters = True


#Add DecayTreeFitter for the Bplus
dtt_BJpsiK.B.addTupleTool('TupleToolDecayTreeFitter/DTF_ConsJpsi')#DTF_ConsJspi is the name we give it, since it is used to constrain Jpsi
dtt_BJpsiK.B.DTF_ConsJpsi.constrainToOriginVertex = True
dtt_BJpsiK.B.DTF_ConsJpsi.Verbose = True
dtt_BJpsiK.B.DTF_ConsJpsi.daughtersToConstrain = ['J/psi(1S)'] #this one takes a list
dtt_BJpsiK.B.DTF_ConsJpsi.UpdateDaughters = True

#Add DecayTreeFitter for the Bplus
dtt_BJpsiPi.B.addTupleTool('TupleToolDecayTreeFitter/DTF_ConsJpsi')#DTF_ConsJspi is the name we give it, since it is used to constrain Jpsi
dtt_BJpsiPi.B.DTF_ConsJpsi.constrainToOriginVertex = True
dtt_BJpsiPi.B.DTF_ConsJpsi.Verbose = True
dtt_BJpsiPi.B.DTF_ConsJpsi.daughtersToConstrain = ['J/psi(1S)'] #this one takes a list
dtt_BJpsiPi.B.DTF_ConsJpsi.UpdateDaughters = True




#vertex isolation Tuple Tool https://gitlab.cern.ch/lhcb/Analysis/-/blob/run2-patches/Phys/DecayTreeTuple/src/TupleToolVtxIsoln.cpp?fbclid=IwAR23xTJ8ZbsnIeaRUfdknD6HgUELTigOIGN-cy4fTqoOA5uW0zX1h_QBsyk
#https://github.com/ibab/lhcb-b2dmumu/blob/master/lhcb/cmtuser/DaVinci_v36r5/Phys/DecayTreeTuple/src/TupleToolVtxIsoln.h

VtxIso_BcJpsiPi = dtt_BcJpsiPi.Bc.addTupleTool('TupleToolVtxIsoln')
VtxIso_BcJpsiPi.Verbose = True

VtxIso_BcJpsiK = dtt_BcJpsiK.Bc.addTupleTool('TupleToolVtxIsoln')
VtxIso_BcJpsiK.Verbose = True


#add Bplus
VtxIso_BJpsiK = dtt_BJpsiK.B.addTupleTool('TupleToolVtxIsoln')
VtxIso_BJpsiK.Verbose = True

VtxIso_BJpsiPi = dtt_BJpsiPi.B.addTupleTool('TupleToolVtxIsoln')
VtxIso_BJpsiPi.Verbose = True


#Bc

for t in [dtt_BcJpsiPi.Bc, dtt_BcJpsiPi.Jpsi, dtt_BcJpsiPi.pi, dtt_BcJpsiPi.mup, dtt_BcJpsiPi.mum]:
    mctruth_BcJpsiPi = t.addTupleTool('TupleToolMCTruth/mctruth')
    mctruth_BcJpsiPi.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
    backgroundinfo = t.addTupleTool("TupleToolMCBackgroundInfo/mcbkg")

for t in [dtt_BcJpsiK.Bc, dtt_BcJpsiK.Jpsi, dtt_BcJpsiK.K, dtt_BcJpsiK.mup, dtt_BcJpsiK.mum]:
    mctruth_BcJpsiK = t.addTupleTool('TupleToolMCTruth/mctruth')
    mctruth_BcJpsiK.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
    backgroundinfo = t.addTupleTool("TupleToolMCBackgroundInfo/mcbkg")

for t in [dtt_BJpsiK.B, dtt_BJpsiK.Jpsi, dtt_BJpsiK.K, dtt_BJpsiK.mup, dtt_BJpsiK.mum]:
    mctruth_BJpsiK = t.addTupleTool('TupleToolMCTruth/mctruth')
    mctruth_BJpsiK.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
    backgroundinfo = t.addTupleTool("TupleToolMCBackgroundInfo/mcbkg")

for t in [dtt_BJpsiPi.B, dtt_BJpsiPi.Jpsi, dtt_BJpsiPi.pi, dtt_BJpsiPi.mup, dtt_BJpsiPi.mum]:
    mctruth_BJpsiPi = t.addTupleTool('TupleToolMCTruth/mctruth')
    mctruth_BJpsiPi.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
    backgroundinfo = t.addTupleTool("TupleToolMCBackgroundInfo/mcbkg")

#mctruth_BJpsiPi = dtt_BJpsiPi.Jpsi.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BcJpsiPi.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
#
#
#mctruth_BcJpsiPi = dtt_BcJpsiPi.pi.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BcJpsiPi.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
#
#
#mctruth_BcJpsiPi = dtt_BcJpsiPi.mup.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BcJpsiPi.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
#
#
#mctruth_BcJpsiPi = dtt_BcJpsiPi.mum.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BcJpsiPi.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
#
#





#backgroundinfo = dtt_BcJpsiPi.addTupleTool("TupleToolMCBackgroundInfo")
#    backgroundinfo_plus = dttplus.addTupleTool("TupleToolMCBackgroundInfo")


#mctruth_BcJpsiPi = dtt_BcJpsiPi.Bc.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BcJpsiPi.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]

#mctruth_BcJpsiPi = dtt_BcJpsiPi.Bc.addTupleTool('TupleToolMCTruth')
#mctruth_BcJpsiPi = dtt_BcJpsiPi.Bc.addTupleTool('TupleToolMCBackgroundInfo')
#
##Bplus
#mctruth_BJpsiK = dtt_BJpsiK.B.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BJpsiK.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
#mctruth_BcJpsiK = dtt_BcJpsiK.Bc.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BcJpsiK.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
#
##Bplus
#mctruth_BJpsiPi = dtt_BJpsiPi.B.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BJpsiPi.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]




TCKList = [ "0x1600","0x1601","0x1602","0x1603","0x1604","0x1605","0x1606","0x1607","0x1608","0x1609","0x160A","0x160B","0x160C","0x160D","0x160E","0x160F"]

L0_2016 = [f"0x16{i}" for i in "01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 11 12 13".split()]
L0_2017 = [f"0x17{i}" for i in "01 02 03 04 05 06 A2 A3 A4 A5 A6 B0 B1 07 A7 08 A8 09 A9 20 21 22 23 24".split()]

L0_2018 = [f"0x18{i}" for i in "01 A1 A2 03 A3 A4 20 21 22 23 24 25 26 27 30 31 32".split()]
for t in [dtt_BcJpsiPi, dtt_BcJpsiK, dtt_BJpsiK, dtt_BJpsiPi]:
    L0_tool = t.addTupleTool('TupleToolL0Data/L0Data')
    L0_tool.TCKList = L0_2016 + L0_2017 + L0_2018
    L0_tool.DataList = ["ALL"]
    L0_tool.ScaledData = False
#Truth matching algorithms from Tom Hadavizadeh (25/11/2022)

from Configurables import TrackAssociator
velotrassoc = TrackAssociator()
velotrassoc.TracksInContainer = 'Rec/Track/FittedHLT1VeloTracks'

from Configurables import LoKi__Track2MC
lokitrassoc = LoKi__Track2MC()
lokitrassoc.Tracks = ['Rec/Track/FittedHLT1VeloTracks']

from Configurables import LoKi__PV2MC
pv2mc = LoKi__PV2MC()
pv2mc.MCVertices = 'MC/Vertices'
pv2mc.Primaries =  'Rec/Vertex/Primary'


#Added by Dr Jake Lane

mc_basic_loki_vars = {
    'ETA': 'MCETA',
    'PHI': 'MCPHI',
    'PT': 'MCPT',
    'PX': 'MCPX',
    'PY': 'MCPY',
    'PZ': 'MCPZ',
    'E': 'MCE',
    'P': 'MCP',
    
}


#Added by Dr Jake Lane
decay_BcJpsiPi = "[B_c+ ==> J/psi(1S) ^pi+]CC"
mctuple_BcJpsiPi = MCDecayTreeTuple("BcJpsiPiMC")
mctuple_BcJpsiPi.Decay = decay_BcJpsiPi
mctuple_BcJpsiPi.setDescriptorTemplate("${Bc}[B_c+ ==> ${Jpsi}(J/psi(1S) ==> ${mum}mu- ${mup}mu+) ${pi}pi+]CC")
mctuple_BcJpsiPi.ToolList = []
mctuple_BcJpsiPi.addTupleTool(
            'LoKi::Hybrid::MCTupleTool/basicLoKiTT'
           ).Variables = mc_basic_loki_vars

decay_BcJpsiK = "[B_c+ ==> J/psi(1S) ^K+]CC"
mctuple_BcJpsiK = MCDecayTreeTuple("BcJpsiKMC")
mctuple_BcJpsiK.Decay = decay_BcJpsiK
mctuple_BcJpsiK.setDescriptorTemplate("${Bc}[B_c+ ==> ${Jpsi}(J/psi(1S) ==> ${mum}mu- ${mup}mu+) ${K}K+]CC")
mctuple_BcJpsiK.ToolList = []
mctuple_BcJpsiK.addTupleTool(
            'LoKi::Hybrid::MCTupleTool/basicLoKiTT'
            ).Variables = mc_basic_loki_vars





decay_BJpsiK = "[B+ ==> J/psi(1S) ^K+]CC"
mctuple_BJpsiK = MCDecayTreeTuple("BJpsiKMC")
mctuple_BJpsiK.Decay = decay_BJpsiK
mctuple_BJpsiK.setDescriptorTemplate("${B}[B+ ==> ${Jpsi}(J/psi(1S) ==> ${mum}mu- ${mup}mu+) ${K}K+]CC")
mctuple_BJpsiK.ToolList = []
mctuple_BJpsiK.addTupleTool(
            'LoKi::Hybrid::MCTupleTool/basicLoKiTT'
            ).Variables = mc_basic_loki_vars

decay_BJpsiPi = "[B+ ==> J/psi(1S) ^pi+]CC"
mctuple_BJpsiPi = MCDecayTreeTuple("BJpsiPiMC")
mctuple_BJpsiPi.Decay = decay_BJpsiPi
mctuple_BJpsiPi.setDescriptorTemplate("${B}[B+ ==> ${Jpsi}(J/psi(1S) ==> ${mum}mu- ${mup}mu+) ${pi}pi+]CC")
mctuple_BJpsiPi.ToolList = []
mctuple_BJpsiPi.addTupleTool(
            'LoKi::Hybrid::MCTupleTool/basicLoKiTT'
            ).Variables = mc_basic_loki_vars


mctuple_BcJpsiPi.addTupleTool('TupleToolPVTrackInfo')
mctuple_BcJpsiPi.addTupleTool('TupleToolRecoStats')
mctuple_BJpsiK.addTupleTool('TupleToolPVTrackInfo')
mctuple_BJpsiK.addTupleTool('TupleToolRecoStats')
mctuple_BcJpsiK.addTupleTool('TupleToolPVTrackInfo')
mctuple_BcJpsiK.addTupleTool('TupleToolRecoStats')
mctuple_BJpsiPi.addTupleTool('TupleToolPVTrackInfo')
mctuple_BJpsiPi.addTupleTool('TupleToolRecoStats')










#add MC truth information
#mctruth_BcJpsiPi = dtt_BcJpsiPi.Bc.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BcJpsiPi.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
#mctruth_BJpsiK = dtt_BJpsiK.B.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BJpsiK.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
#mctruth_BJpsiPi = dtt_BJpsiPi.B.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BJpsiPi.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]
#mctruth_BcJpsiK = dtt_BcJpsiK.Bc.addTupleTool('TupleToolMCTruth/mctruth')
#mctruth_BcJpsiK.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]




#Truth matching algorithms from Tom Hadavizadeh (25/11/2022)

from Configurables import TrackAssociator
velotrassoc = TrackAssociator()
velotrassoc.TracksInContainer = 'Rec/Track/FittedHLT1VeloTracks'

from Configurables import LoKi__Track2MC
lokitrassoc = LoKi__Track2MC()
lokitrassoc.Tracks = ['Rec/Track/FittedHLT1VeloTracks']

from Configurables import LoKi__PV2MC
pv2mc = LoKi__PV2MC()
pv2mc.MCVertices = 'MC/Vertices'
pv2mc.Primaries =  'Rec/Vertex/Primary'

#new algorithms
#DaVinci().UserAlgorithms += [B_seq.sequence(),velotrassoc,lokitrassoc,  dtt, etuple]
'''
from Configurables import PatPV2DFit3D, PVOfflineTool

myPV2DFit3D = PatPV2DFit3D("myPV2DFit3D")
myPV2DFit3D.addTool(PVOfflineTool)
myPV2DFit3D.PVOfflineTool.PVFitterName = "LSAdaptPV3DFitter"
myPV2DFit3D.PVOfflineTool.PVsChi2Separation = x
myPV2DFit3D.OutputVerticesName = "Rec/Vertex/myPV2DFit3D"
'''

#old algorithms
DaVinci().UserAlgorithms += [BcJpsiPi_seq.sequence(), BcJpsiK_seq.sequence(), BJpsiK_seq.sequence(), BJpsiPi_seq.sequence(), dtt_BcJpsiPi, dtt_BcJpsiK, dtt_BJpsiK, dtt_BJpsiPi, mctuple_BcJpsiPi, mctuple_BcJpsiK, mctuple_BJpsiK, mctuple_BJpsiPi, etuple]


#from Configurables import DaVinci
DaVinci().InputType = "LDST"
DaVinci().TupleFile = "/eos/lhcb/user/j/jolane/BcMPI/selection/test/BcJpsiPi_MC_2016_MagUp.root"
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
DaVinci().CondDBtag = "sim-20170721-2-vc-mu100"
DaVinci().DDDBtag = "dddb-20170721-3"

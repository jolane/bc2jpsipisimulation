#!/usr/bin/env bash
source /cvmfs/lhcb.cern.ch/lib/LbEnv
lb-run -c best Boole/v33r3 gaudirun.py step2.py
lb-run -c best Moore/v28r3p1 gaudirun.py step3.py
lb-run -c best Moore/v28r3p1 gaudirun.py step4.py
lb-run -c best Moore/v28r3p1 gaudirun.py step5.py
lb-run -c best Brunel/v54r4 gaudirun.py step6.py
lb-run -c best DaVinci/v44r7 gaudirun.py step7.py
lb-run -c best DaVinci/v44r7 gaudirun.py step8.py

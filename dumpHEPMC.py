import sys

import GaudiPython as GP
from GaudiConf import IOHelper
from Configurables import DaVinci
import ROOT
dv = DaVinci()
dv.DataType = '2016'
dv.Simulation = True
import os
inputFile = sys.argv[-1]
IOHelper('ROOT').inputFiles([inputFile])

appMgr = GP.AppMgr()
evt = appMgr.evtsvc()

#appMgr.run(1)

def writeMC(evt, fileName):

    hepMC = evt["/Event/Gen/HepMCEvents"] 
    ge = hepMC[0].pGenEvt() 
    ge.write(ROOT.std.ofstream(fileName, ROOT.std.ios_base.app))


n = 5

fileName = inputFile.split(".")[0] + ".hepmc"
outString = """HepMC::Version 3.02.04
HepMC::Asciiv3-START_EVENT_LISTING
"""
for i in range(n):
    appMgr.run(1)
    evt.dump()
    writeMC(evt, fileName + f".{i+1}")
    os.system(f"sed -ie 's/E 1/E {i+1}/g' {fileName}.{i+1}")
    os.system(f"rm {fileName}.{i+1}e")
    outString += "".join(l for l in open(f"{fileName}.{i+1}"))

    os.system(f"rm {fileName}.{i+1}")

outString += "HepMC::Asciiv3-END_EVENT_LISTING"
open(fileName, "w").write(outString)

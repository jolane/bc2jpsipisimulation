
from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/DaVinci/DV-Stripping34-Stripping-MC-NoPrescaling-DST.py")
importOptions("$APPCONFIGOPTS/DaVinci/DataType-2018.py")
importOptions("$APPCONFIGOPTS/DaVinci/InputType-DST.py")
importOptions("$APPCONFIGOPTS/DaVinci/LDST-Output.py")
from GaudiConf import IOHelper

#Options: $APPCONFIGOPTS/DaVinci/DV-Stripping34-Stripping-MC-NoPrescaling-LDST.py;$APPCONFIGOPTS/DaVinci/DataType-2018.py;$APPCONFIGOPTS/DaVinci/InputType-DST.py;$APPCONFIGOPTS/DaVinci/LDST-Output.py Options format: Multicore: N

from Configurables import LHCbApp, DaVinci
inputfile = "Tesla.dst"
from GaudiConf import IOHelper
#Brunel().output_file = "brunel.dst"
IOHelper('ROOT').inputFiles([inputfile])
#



###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import Generation

Pythia8SoftQCDNonDiffractive = ["SoftQCD:all = off",
                                "SoftQCD:nonDiffractive = on"]


gen = Generation("Generation")

## Overwrite the chosen tool to always use the Special tool
gen.SampleGenerationTool = "Special"

gen.Special.Pythia8Production.UserHooks += ["InclusivebHook"]
gen.Special.Pythia8Production.Commands += Pythia8SoftQCDNonDiffractive

## Default value of pTHat threshold for Inclusive b simulation is 4.00GeV.
## To change the default value, you can uncomment the lines below.
pTHatValueInclusiveb = ["InclusivebHook:pTHatThreshold = 4.50"]
gen.Special.Pythia8Production.Commands += pTHatValueInclusiveb

# Request a B0 in the acceptance by reusing the Bc cut tool
evt_type = gen.EventType
gen.Special.CutTool = "BcDaughtersInLHCb"
from Configurables import BcDaughtersInLHCb
gen.Special.addTool(BcDaughtersInLHCb)
if(  evt_type//1000000==11):
    gen.Special.BcDaughtersInLHCb.BcPdgId = 511
elif(evt_type//1000000==12):
    gen.Special.BcDaughtersInLHCb.BcPdgId = 521
elif(evt_type//1000000==13):
    gen.Special.BcDaughtersInLHCb.BcPdgId = 531
elif(evt_type//1000000==14):
    gen.Special.BcDaughtersInLHCb.BcPdgId = 541
elif(evt_type//1000000==15):
    gen.Special.BcDaughtersInLHCb.BcPdgId = 5122
    # for general flag 1, selection flag 6, check 7th digit
elif(evt_type//1000000==16 and (evt_type//10 - 10*(evt_type//100) == 0 )):     
    gen.Special.BcDaughtersInLHCb.BcPdgId = 5112 # Simgab-
elif(evt_type//1000000==16 and (evt_type//10 - 10*(evt_type//100) == 1 )):     
    gen.Special.BcDaughtersInLHCb.BcPdgId = 5212 # Simgab0
elif(evt_type//1000000==16 and (evt_type//10 - 10*(evt_type//100) == 2 )):     
    gen.Special.BcDaughtersInLHCb.BcPdgId = 5222 # Simgab+
elif(evt_type//1000000==16 and (evt_type//10 - 10*(evt_type//100) == 3 )):     
    gen.Special.BcDaughtersInLHCb.BcPdgId = 5132 # Xib-
elif(evt_type//1000000==16 and (evt_type//10 - 10*(evt_type//100) == 4 )):     
    gen.Special.BcDaughtersInLHCb.BcPdgId = 5232 # Xib0
elif(evt_type//1000000==16 and (evt_type//10 - 10*(evt_type//100) == 5 )):     
    gen.Special.BcDaughtersInLHCb.BcPdgId = 5332 # Omegab-

#Boole-v33r3
from Gaudi.Configuration import *
importOptions("$APPCONFIGOPTS/Boole/Default.py")
importOptions("$APPCONFIGOPTS/Boole/EnableSpillover.py")
importOptions("$APPCONFIGOPTS/Boole/DataType-2015.py")
importOptions("$APPCONFIGOPTS/Boole/Boole-SetOdinRndTrigger.py")
import pickle as pkl
dddb = "dddb-20170721-3"
condb = "sim-20190128-vc-md100"



from GaudiConf import IOHelper
from GaudiConf import IOHelper; IOHelper("ROOT","ROOT").postConfigServices()

import glob
from GaudiConf import IOExtension
#IOHelper("ROOT").inputFiles([
#     "LFN:/lhcb/user/j/jolane/2023_06/753214/753214084/Gauss-14143013-100ev-20230606.xsim",
#     "LFN:/lhcb/user/j/jolane/2023_06/753214/753214085/Gauss-14143013-100ev-20230606.xsim",
#     "LFN:/lhcb/user/j/jolane/2023_06/753214/753214086/Gauss-14143013-100ev-20230606.xsim",
#     "LFN:/lhcb/user/j/jolane/2023_06/753214/753214087/Gauss-14143013-100ev-20230606.xsim",
#     "LFN:/lhcb/user/j/jolane/2023_06/753214/753214088/Gauss-14143013-100ev-20230606.xsim",
#     "LFN:/lhcb/user/j/jolane/2023_06/753214/753214089/Gauss-14143013-100ev-20230606.xsim",
#     "LFN:/lhcb/user/j/jolane/2023_06/753214/753214090/Gauss-14143013-100ev-20230606.xsim",
#     "LFN:/lhcb/user/j/jolane/2023_06/753214/753214091/Gauss-14143013-100ev-20230606.xsim",
#     "LFN:/lhcb/user/j/jolane/2023_06/753214/753214092/Gauss-14143013-100ev-20230606.xsim",
#     ], clear=True)
#

#inputfile = "Gauss-14103031-5ev-20230404.sim"
#inputfile = "Gauss-14143013-5ev-20230405.sim"
inputfiles = list(glob.glob("*.xsim"))
#inputfiles = pkl.load(open("job1_urls.pkl", "rb"))
#inputfile = "Gauss-14143013-5ev-20230412.sim"
#inputfiles = open("urls_job_1.txt").read().split()
IOHelper('ROOT').inputFiles(inputfiles)
#Boole().DatasetName = "Gauss-14113021-5ev-20230330"
#
#
from Configurables import LHCbApp, Boole

#LHCbApp().DDDBtag = 'dddb-20170721-3'
#LHCbApp().CondDBtag = 'sim-20170721-2-vc-md100'


#LHCbApp().CondDBtag ='upgrade/sim-20190912-vc-md100'
#LHCbApp().DDDBtag =  'upgrade/dddb-20190912'



LHCbApp().CondDBtag = condb
LHCbApp().DDDBtag =  dddb



LHCbApp().EvtMax = -1

LHCbApp().DataType="2016"


#output = "Gauss-{}-5ev-20230329.xgen".format(eventnumber)
#output = "Gauss.xgen"
#print(output)

#Boole().DatasetName = inputfile.replace('Gauss','Boole').replace('.sim','')




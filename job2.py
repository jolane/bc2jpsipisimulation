#Boole-v33r3

job1 = jobs(43)
subjobs1 = []
for s in job1.subjobs.select(status="completed"):
    try:
        url = s.outputfiles[0].accessURL()[0]
        subjobs1 += [s]
    except Exception as err:
        print(f"{err} for {s.id}")

j = Job(name = "Boole")
try:
    myApp = prepareGaudiExec('Boole', 'v33r3', myPath='.')
except:
    myApp = GaudiExec()
    myApp.directory='./BooleDev_v33r3'
#
j.application = myApp
j.application.options = ["step2.py"]
j.application.platform = 'x86_64-centos7-gcc9-do0'
j.inputdata = [s.outputfiles[0] for s in subjobs1]
j.outputfiles = [DiracFile("*.digi"), DiracFile("*.root")]
j.backend = Interactive()

#j.application.args=["--option='from GaudiConf import IOHelper; IOHelper("+'"ROOT", "ROOT"'+ ").postConfigServices()'"] 

j.submit()

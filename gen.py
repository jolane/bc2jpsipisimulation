j = Job(name="Gauss Generation" )
myApp = GaudiExec()
myApp.directory="./GaussDev_v56r3"
#myApp = prepareGaudiExec('Gauss','v56r3', myPath='.')
j.application = myApp
j.application.options = ["step1.py"]
#j.application.platform = 'x86_64-centos7-gcc8-opt'
j.application.platform = 'x86_64_v2-centos7-gcc11-opt'

#System config: x86_64_v2-centos7-gcc11-opt MC TCK:
#j.application.platform = 'x86_64-slc6-gcc48-opt'
#j.inputfiles = [LocalFile("step1.py"), LocalFile("LHCbDefault.cmd"), LocalFile("BcVegPyPythia8.py"), LocalFile("Pythia8_IncbUserHook.py")]
j.inputfiles = [LocalFile("14143013.py"), LocalFile("Bc_Jpsipi,mm=BcVegPy,NoCut.dec")]
#14143013.py  Bc_Jpsipi,mm=BcVegPy,NoCut.dec
j.outputfiles = [LocalFile("*.sim")]
j.backend = Interactive()
j.submit()
